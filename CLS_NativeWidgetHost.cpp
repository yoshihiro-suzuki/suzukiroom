// Fill out your copyright notice in the Description page of Project Settings.

#include "CLS_NativeWidgetHost.h"

#include "Colors/SColorSpectrum.h"
#include "Kismet/KismetMathLibrary.h"

UCLS_ColorPicker::UCLS_ColorPicker(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TSharedRef<SColorSpectrum> colorPicker = SNew(SColorSpectrum)
		.SelectedColor_Lambda([this]()
	{
		return SelectedColorHSV;
	})
		.OnValueChanged_Lambda([this](FLinearColor c)
	{
		SelectedColorHSV = c;
		UKismetMathLibrary::HSVToRGB_Vector(SelectedColorHSV, SelectedColorRGB);
	});

	SetContent(colorPicker);
}