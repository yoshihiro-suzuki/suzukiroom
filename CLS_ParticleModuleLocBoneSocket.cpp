// Fill out your copyright notice in the Description page of Project Settings.


#include "CLS_ParticleModuleLocBoneSocket.h"
#include "Engine.h"
#include "Rendering/SkeletalMeshLODRenderData.h"
#include "Rendering/SkeletalMeshRenderData.h"
//#include "Animation/Skeleton.h"

class FSkeletalMeshRenderData;
class FSkinWeightVertexBuffer;

bool UCLS_ParticleModuleLocBoneSocket::PL_CheckSourceIndex(FModuleLocationBoneSocketInstancePayload* Payload, USkeletalMeshComponent* SourceComponent, int32 BoneIndex)
{

	if (SourceComponent->GetBoneName(BoneIndex) != FName(TEXT("root")))
	{
		if (BoneIndex < 3) return false;
		return true;

	}

	/*
		SourceComponent->SkeletalMesh->name
		SourceComponent->SkeletalMesh->CalculateRequiredBones()
		SourceComponent->GetBoneName()
*/

	return false;
}

int32 UCLS_ParticleModuleLocBoneSocket::PL_SelectNextSpawnIndex(FModuleLocationBoneSocketInstancePayload* InstancePayload, USkeletalMeshComponent* SourceComponent)
{
	check(InstancePayload && SourceComponent);

	int32 SourceIndex = -1;
	int32 MaxIndex = GetMaxSourceIndex(InstancePayload, SourceComponent);

	//If we're selecting from a pre generated list then always select sequentially, randomness will be introduced when generating the list.
	if (SelectionMethod == BONESOCKETSEL_Sequential || SourceIndexMode == EBoneSocketSourceIndexMode::PreSelectedIndices)
	{
		// Simply select the next socket
		SourceIndex = InstancePayload->LastSelectedIndex++;
		if (InstancePayload->LastSelectedIndex >= MaxIndex)
		{
			InstancePayload->LastSelectedIndex = 0;
		}
	}
	else if (SelectionMethod == BONESOCKETSEL_Random)
	{
		FSkeletalMeshRenderData* SkeletalRenderData = SourceComponent->SkeletalMesh->GetResourceForRendering();

		const TArray<FMeshBoneInfo> &Bones = SourceComponent->SkeletalMesh->Skeleton->GetReferenceSkeleton().GetRawRefBoneInfo();

		check(SkeletalRenderData);

		FSkeletalMeshLODRenderData* RenderingLOD = &SkeletalRenderData->LODRenderData[0];
		
		if (RenderingLOD->ActiveBoneIndices.Num() < 3)
		{
			return INDEX_NONE;
		}

		int32 ActiveIndex = FMath::RandRange(3, RenderingLOD->ActiveBoneIndices.Num()-1);


		//stack over flow 
		if (ActiveIndex >= RenderingLOD->ActiveBoneIndices.Num() )
		{
			UE_LOG(LogTemp, Warning, TEXT("ParticleModuleLocBoneSocket Stack OverFlow") );
			return INDEX_NONE;
		}

		SourceIndex = RenderingLOD->ActiveBoneIndices[ActiveIndex];

		/*if (SourceIndex == 21 || SourceIndex == 22 || SourceIndex == 23)
		{
			UE_LOG(LogTemp, Warning, TEXT("ParticleModuleLocBoneSocket Invalid BoneIndex"));
		}*/

		InstancePayload->LastSelectedIndex = SourceIndex;
#if 0
		//FString DebugString = FString::Printf(TEXT("%s"), *Bones[SourceIndex].Name.ToString());
		//GEngine->AddOnScreenDebugMessage((uint64)0, 5.0f, FColor::Emerald, DebugString);
#endif
	}

	if (SourceIndex == -1)
	{
		return INDEX_NONE;
	}
	if (SourceIndex >= MaxIndex)
	{
		return INDEX_NONE;
	}

	return SourceIndex;
}


void UCLS_ParticleModuleLocBoneSocket::Spawn(FParticleEmitterInstance* Owner, int32 Offset, float SpawnTime, FBaseParticle* ParticleBase)
{
	///SCOPE_CYCLE_COUNTER(STAT_ParticleSkelMeshSurfTime);


	FModuleLocationBoneSocketInstancePayload* InstancePayload =
		(FModuleLocationBoneSocketInstancePayload*)(Owner->GetModuleInstanceData(this));
	if (InstancePayload == NULL)
	{
		return;
	}

	if (!InstancePayload->SourceComponent.IsValid())
	{
		// Setup the source skeletal mesh component...
		//
		//@CHG 4.20
		//USkeletalMeshComponent* SkeletalMeshComponent = GetSkeletalMeshComponentSource(Owner);
		GetSkeletalMeshComponentSource(Owner, InstancePayload);
		USkeletalMeshComponent* SkeletalMeshComponent = InstancePayload->SourceComponent.Get();

		if (SkeletalMeshComponent != NULL)
		{
			InstancePayload->SourceComponent = SkeletalMeshComponent;
			RegeneratePreSelectedIndices(InstancePayload, SkeletalMeshComponent);
		}
		else
		{
			return;
		}
	}

	// Early out if source component is still invalid 
	if (!InstancePayload->SourceComponent.IsValid())
	{
		return;
	}
	USkeletalMeshComponent* SourceComponent = InstancePayload->SourceComponent.Get();

	//@sub int32 SourceIndex = SelectNextSpawnIndex(InstancePayload, SourceComponent);
	int32 SourceIndex = PL_SelectNextSpawnIndex(InstancePayload, SourceComponent); //@add
	
	if (SourceIndex == INDEX_NONE)
	{
		return;
	}

	FVector SourceLocation;
	FQuat RotationQuat;
	const int32 MeshRotationOffset = Owner->GetMeshRotationOffset();
	const bool bMeshRotationActive = MeshRotationOffset > 0 && Owner->IsMeshRotationActive();
	FQuat* SourceRotation = (bMeshRotationActive) ? NULL : &RotationQuat;
	if (GetParticleLocation(InstancePayload, Owner, SourceComponent, SourceIndex, SourceLocation, SourceRotation) == true)
	{
		SPAWN_INIT
		{
			FModuleLocationBoneSocketParticlePayload* ParticlePayload = (FModuleLocationBoneSocketParticlePayload*)((uint8*)&Particle + Offset);
		ParticlePayload->SourceIndex = SourceIndex;
		Particle.Location = SourceLocation;
		ensureMsgf(!Particle.Location.ContainsNaN(), TEXT("NaN in Particle Location. Template: %s, Component: %s"), Owner->Component ? *GetNameSafe(Owner->Component->Template) : TEXT("UNKNOWN"), *GetPathNameSafe(Owner->Component));
		if (InheritingBoneVelocity())
		{
			// Set the base velocity for this particle.
			Particle.BaseVelocity = FMath::Lerp(Particle.BaseVelocity, InstancePayload->BoneSocketVelocities[SourceIndex], InheritVelocityScale);
			ensureMsgf(!Particle.BaseVelocity.ContainsNaN(), TEXT("NaN in Particle Base Velocity. Template: %s, Component: %s"), Owner->Component ? *GetNameSafe(Owner->Component->Template) : TEXT("UNKNOWN"), *GetPathNameSafe(Owner->Component));
		}
		if (bMeshRotationActive)
		{
			FMeshRotationPayloadData* PayloadData = (FMeshRotationPayloadData*)((uint8*)&Particle + MeshRotationOffset);
			PayloadData->Rotation = RotationQuat.Euler();
//////@sub			if (Owner->CurrentLODLevel->RequiredModule->bUseLocalSpace == true)
			{
				PayloadData->Rotation = Owner->Component->GetComponentTransform().InverseTransformVectorNoScale(PayloadData->Rotation);
			}
		}
		}
	}
}
