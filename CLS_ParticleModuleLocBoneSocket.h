// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Particles/Location/ParticleModuleLocationBoneSocket.h"
#include "CLS_ParticleModuleLocBoneSocket.generated.h"

/**
 * 
 */
UCLASS(editinlinenew, hidecategories = Object, meta = (DisplayName = "PL/Bone/Socket Location"))
class PRINCELAND_API UCLS_ParticleModuleLocBoneSocket : public UParticleModuleLocationBoneSocket
{
	GENERATED_BODY()

	
	bool  PL_CheckSourceIndex(FModuleLocationBoneSocketInstancePayload* Payload, USkeletalMeshComponent* SourceComponent, int32 BoneIndex);
	int32 PL_SelectNextSpawnIndex(FModuleLocationBoneSocketInstancePayload* InstancePayload, USkeletalMeshComponent* SourceComponent);
	virtual void	Spawn(FParticleEmitterInstance* Owner, int32 Offset, float SpawnTime, FBaseParticle* ParticleBase) override;

	
	
};
