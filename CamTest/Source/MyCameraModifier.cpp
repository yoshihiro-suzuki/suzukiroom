// Fill out your copyright notice in the Description page of Project Settings.

#include "MyCameraModifier.h"




UMyCameraModifier::UMyCameraModifier()
{
	m_fCustomFOV = 0.0f;
	m_fDefaultFOV = 90.0f;
}

bool UMyCameraModifier::ModifyCamera(float DeltaTime, FMinimalViewInfo& InOutPOV)
{
	if (m_fDefaultFOV == 0.0f)
	{
		m_fDefaultFOV = InOutPOV.FOV;
	}

	InOutPOV.FOV = m_fCustomFOV;
	return true;
}