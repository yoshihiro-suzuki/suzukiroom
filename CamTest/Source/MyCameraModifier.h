// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraModifier.h"
#include "MyCameraModifier.generated.h"

/**
 * 
 */
UCLASS()
class CAMTEST_API UMyCameraModifier : public UCameraModifier
{
	GENERATED_BODY()

public:

	UMyCameraModifier();
	
	virtual bool ModifyCamera(float DeltaTime, struct FMinimalViewInfo& InOutPOV);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float m_fCustomFOV;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float m_fDefaultFOV;
};
